/* This file sets up a data model for projects. */

#include "httpd.h"

#include <tree.h>

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "db_xml.h"
#include "style.h"
#include "util.h"
#include "auth.h"
#include "xml_util.h"
#include "db_ops.h"
#include "certs.h"
#include "schema.h"

#include "proj.h"

/* Schema for project page. */

static SchemaField proj_fields[] = {
  { "name", "Project name", "proj/", 20, 0 },
  { "url", "Project homepage URL", NULL, 60, 0 },
  { "fmurl", "<a href=\"http://freshmeat.net/\">Freshmeat</a> URL", NULL, 60, 0 },
  { "license", "License", NULL, 20, 0 },
  { "notes", "Notes", NULL, 60016, SCHEMA_TEXTAREA },
  { NULL }
};

static char *staff_rels[] = {
  "None",
  "Helper",
  "Documenter",
  "Contributor",
  "Developer",
  "Lead Developer",
  NULL
};

static SchemaField staff_fields[] = {
  { "name", "Project name", "proj/", 20, 0 },
  { "person", "Person", "acct/", 20, 0 },
  { "type", "Type of relationship", NULL, 20, SCHEMA_SELECT, staff_rels }
};

static DbField staff_db_fields[] = {
  { "name", "proj/", DB_FIELD_INDEX | DB_FIELD_UNIQUE },
  { "person", "acct/", DB_FIELD_INDEX | DB_FIELD_UNIQUE },
  { "type", NULL, 0 }
};

static DbRelation staff_db_rel = {
  "staff",
  sizeof (staff_db_fields) / sizeof (staff_db_fields[0]),
  staff_db_fields,
  0
};

/**
 * proj_url: Get a url for the project name.
 * @proj: The project name.
 *
 * Return value: A url for the project
 **/
static char *
proj_url (VirguleReq *vr, const char *proj)
{
  pool *p = vr->r->pool;
  return ap_pstrcat (p, vr->prefix, "/proj/", ap_escape_uri (p, proj), "/", NULL);
}

char *
render_proj_name (VirguleReq *vr, const char *proj)
{
  pool *p = vr->r->pool;

  return ap_pstrcat (p, "<a href=\"", proj_url (vr, proj), "\">", proj,
		     "</a>", NULL);
}

/**
 * proj_ok_to_edit: Determine whether user is ok to edit the project page.
 * @doc: The document for the project.
 *
 * Implements the policy that it is ok to edit if (a) you're certified or
 * (b) it is not locked and you are the creator.
 *
 * Return value: TRUE if ok to edit project.
 **/
static int
proj_ok_to_edit (VirguleReq *vr, xmlDoc *doc)
{
  pool *p = vr->r->pool;
  xmlNode *tree;
  char *creator;
  char *locked;

  auth_user (vr);

  if (vr->u == NULL)
    return 0;

  if (strcmp (req_get_tmetric_level (vr, vr->u),
		  cert_level_to_name (CERT_LEVEL_NONE)))
    return 1;

  tree = xml_find_child (doc->root, "info");

  locked = xml_get_prop (p, tree, "locked");
  if (locked && !strcmp (locked, "yes"))
    return 0;
  creator = xml_get_prop (p, tree, "creator");

  return (!strcmp (vr->u, creator));
}

static int
proj_new_serve (VirguleReq *vr)
{
  
/* This should really be in XML. But two things:

     First, it's dependent on being authenticated (but not necessarily
     certified).

     Second, all these forms should be schema based and mostly automated.

     But for the time being, the path of least resistance is to write
     a pile of C. So here goes.
*/
  pool *p = vr->r->pool;
  Buffer *b = vr->b;
  const char *fields[] = { "name", "url", "fmurl", "license", "notes", NULL };

  auth_user (vr);

  if (vr->u == NULL)
    return send_error_page (vr, "Not logged in", "You can't create a new project page because you're not logged in.");

  render_header (vr, "Create new project page");

  buffer_puts (b, "<p> Create a new project page: </p>\n"
	       "<form method=\"POST\" action=\"newsub.html\">\n");

  schema_render_inputs (p, b, proj_fields, fields, NULL);


  buffer_puts (b, " <p> <input type=\"submit\" value=\"Create\">\n"
	       "</form>\n");

  render_acceptable_html (vr);

  return render_footer_send (vr);
  
}

static int
proj_newsub_serve (VirguleReq *vr)
{
  pool *p = vr->r->pool;
  Db *db = vr->db;
  table *args;
  const char *date;
  const char *name, *url, *fmurl, *license, *notes;
  char *db_key;
  xmlDoc *doc;
  xmlNode *root, *tree;
  int status;

  auth_user (vr);

  if (vr->u == NULL)
    return send_error_page (vr, "Not logged in", "You can't create a new project page because you're not logged in.");

  args = get_args_table (vr);

  name = ap_table_get (args, "name");
  url = ap_table_get (args, "url");
  fmurl = ap_table_get (args, "fmurl");
  license = ap_table_get (args, "license");
  notes = ap_table_get (args, "notes");
  date = iso_now (p);

  if (!name[0])
    return send_error_page (vr, "Specify a project name",
			    "You must specify a project name.");

  if (strlen (name) > 30)
    return send_error_page (vr, "Project name too long",
			    "The project name must be 30 characters or less.");

  if (name[0] == '.')
    return send_error_page (vr, "Project name can't begin with dot",
			    "The project can't begin with a dot, sorry.");

  if (strchr (name, '/'))
    return send_error_page (vr, "Project name can't have slash",
			    "The project can't have a slash, sorry.");

  db_key = ap_psprintf (p, "proj/%s/info.xml", name);
  doc = db_xml_get (p, db, db_key);
  if (doc != NULL)
    return send_error_page (vr,
			    "Project already exists",
			    "The project name <tt>%s</tt> already exists.",
			    name);

  doc = db_xml_doc_new (p);

  root = xmlNewDocNode (doc, NULL, "info", NULL);
  doc->root = root;

  tree = xmlNewChild (root, NULL, "cdate", date);
  tree = xmlNewChild (root, NULL, "info", NULL);
  xml_put_prop (p, tree, "url", url);
  xml_put_prop (p, tree, "fmurl", fmurl);
  xml_put_prop (p, tree, "license", license);
  xml_put_prop (p, tree, "notes", notes);
  xml_put_prop (p, tree, "creator", vr->u);
  if (req_ok_to_post (vr))
    xml_put_prop (p, tree, "locked", "yes");

  status = db_xml_put (p, db, db_key, doc);
  if (status)
    return send_error_page (vr,
			    "Error storing project page",
			    "There was an error storing the project page. This means there's something wrong with the site.");

  add_recent (p, db, "recent/proj-c.xml", name, 50);
  add_recent (p, db, "recent/proj-m.xml", name, 50);

  return send_error_page (vr,
			  "Project created",
			  "Project %s created.\n",
			  render_proj_name (vr, name));
}

static int
proj_index_serve (VirguleReq *vr)
{
  Buffer *b = vr->b;
  Db *db = vr->db;
  DbCursor *dbc;
  char *proj;

  auth_user (vr);

  render_header (vr, "Project index");
  buffer_puts (b, "<ul>\n");
  dbc = db_open_dir (db, "proj");

  while ((proj = db_read_dir_raw (dbc)) != NULL)
    {
      buffer_printf (b, "<li>%s\n", render_proj_name (vr, proj));
    }
  buffer_puts (b, "</ul>\n");

  if (vr->u != NULL)
    buffer_puts (b, "<p> <a href=\"new.html\">Create</a> a new project page...</p>\n");
  db_close_dir (dbc);

  return render_footer_send (vr);
}

static int
proj_proj_serve (VirguleReq *vr, const char *path)
{
  Buffer *b = vr->b;
  request_rec *r = vr->r;
  pool *p = r->pool;
  char *q;
  char *name;
  char *db_key;
  char *str;
  xmlDoc *doc, *staff;
  xmlNode *tree;
  char *err;
  char *cdate;
  const char *fields[] = { "type", NULL };
  char *first;
  xmlNode *myrel;

  auth_user (vr);

  q = strchr ((char *)path, '/');
  if (q == NULL)
    {
      ap_table_add (r->headers_out, "Location",
		    ap_make_full_path (r->pool, r->uri, ""));
      return REDIRECT;
    }

  if (q[1] != '\0')
    return send_error_page (vr,
			    "Extra junk",
			    "Extra junk after project name not allowed.");

  name = ap_pstrndup (p, path, q - path);

  db_key = ap_psprintf (p, "proj/%s/info.xml", name);

  doc = db_xml_get (p, vr->db, db_key);
  if (doc == NULL)
    return send_error_page (vr,
			    "Project not found",
			    "Project <tt>%s</tt> was not found.", name);

  str = ap_psprintf (p, "Project info for %s", name);
  render_header (vr, str);

  cdate = xml_find_child_string (doc->root, "cdate", "--no date--");

  tree = xml_find_child (doc->root, "info");
  if (tree != NULL)
    {
      char *url;
      char *fmurl;
      char *notes;
      char *creator;
      char *license;
      char *lastmodby;
      char *mdate;

      creator = xml_get_prop (p, tree, "creator");
      lastmodby = xml_get_prop (p, tree, "lastmodby");

      buffer_printf (b, "<p> Page created %s by <a href=\"%s/person/%s/\">%s</a>",
		     render_date (vr, cdate), vr->prefix, creator, creator);
      if (lastmodby != NULL)
	{
	  mdate = xml_get_prop (p, tree, "mdate");
	  buffer_printf (b, ", last modified %s by <a href=\"%s/person/%s/\">%s</a>",
			 render_date (vr, mdate), vr->prefix,
			 lastmodby, lastmodby);
	}

      buffer_puts (b, ".</p\n");

      url = xml_get_prop (p, tree, "url");
      if (url && url[0])
	{
	  char *url2;
	  char *colon;

	  url2 = url;
	  colon = strchr (url, ':');
	  if (!colon || colon[1] != '/' || colon[2] != '/')
	    url2 = ap_pstrcat (p, "http://", url, NULL);
	  buffer_printf (b, "<p> Homepage: <a href=\"%s\">%s</a> </p>\n",
			 url2, nice_text (p, url));
	}
      /* This should be a subroutine to avoid code duplication. */
      fmurl = xml_get_prop (p, tree, "fmurl");
      if (fmurl && fmurl[0])
	{
	  char *fmurl2;
	  char *colon;

	  fmurl2 = fmurl;
	  colon = strchr (fmurl, ':');
	  if (!colon || colon[1] != '/' || colon[2] != '/')
	    fmurl2 = ap_pstrcat (p, "http://", fmurl, NULL);
	  buffer_printf (b, "<p> Freshmeat page: <a href=\"%s\">%s</a> </p>\n",
			 fmurl2, nice_text (p, fmurl));
	}

      notes = xml_get_prop (p, tree, "notes");
      if (notes && notes[0])
	buffer_printf (b, "<p> <b>Notes:</b> %s </p>\n", nice_htext (vr, notes, &err));

      license = xml_get_prop (p, tree, "license");
      if (license && license[0])
	buffer_printf (b, "<p> License: %s </p>\n", nice_text (p, license));

      /* Render staff listings */
      myrel = NULL;
      first = "<p> This project has the following developers: </p>\n"
	"<ul>\n";
      db_key = ap_psprintf (p, "proj/%s/staff-name.xml", name);

      staff = db_xml_get (p, vr->db, db_key);
      if (staff != NULL)
	{
	  for (tree = staff->root->childs; tree != NULL; tree = tree->next)
	    {
	      char *person;
	      char *type;

	      person = xml_get_prop (p, tree, "person");
	      type = xml_get_prop (p, tree, "type");
	      if (vr->u != NULL && !strcmp (person, vr->u))
		myrel = tree;
	      if (! !strcmp (type, "None"))
		{
		  buffer_puts (b, first);
		  first = "";
		  buffer_printf (b, "<li><a href=\"%s/person/%s/\">%s</a> is a %s.\n",
				 vr->prefix, person, person, type);
		}
	    }
	}
      if (first[0] == 0)
	buffer_puts (b, "</ul>\n");

      if (proj_ok_to_edit (vr, doc))
	{
	  buffer_printf (b, "<p> <a href=\"../edit.html?proj=%s\">Edit...</a> </p>",
			 escape_uri_arg (p, name));
	  buffer_printf (b, "<p> I have the following relation to %s: </p>\n"
		       "<form method=\"POST\" action=\"../relsub.html\">\n"
			 "<input type=\"hidden\" name=\"name\" value=\"%s\">\n",
			 name, name);
	  schema_render_inputs (p, b, staff_fields, fields, myrel);
	  buffer_puts (b, "<input type=\"submit\" value=\"Update\"/>\n"
		       "</form>\n");
	}
    }

  return render_footer_send (vr);
}

static int
proj_edit_serve (VirguleReq *vr)
{
  Buffer *b = vr->b;
  pool *p = vr->r->pool;
  table *args;
  const char *name;
  const char *fields[] = { "url", "fmurl", "license", "notes", NULL };
  char *db_key;
  xmlDoc *doc;
  xmlNode *tree;

  args = get_args_table (vr);
  name = ap_table_get (args, "proj");

  db_key = ap_psprintf (p, "proj/%s/info.xml", name);

  doc = db_xml_get (p, vr->db, db_key);
  if (doc == NULL)
    return send_error_page (vr,
			    "Project not found",
			    "Project <tt>%s</tt> was not found.", name);

  if (!proj_ok_to_edit (vr, doc))
    return send_error_page (vr, "Not authorized",
			    "You are not authorized to edit project %s. You have to either be certified to Apprentice level or higher, or be the creator of the project page and not have anyone else edit the page before you.", name);

  render_header (vr, "Edit project page");

  buffer_printf (b, "<p> Edit project page for %s: </p>\n"
	       "<form method=\"POST\" action=\"editsub.html\">\n"
		 "<input type=\"hidden\" name=\"name\" value=\"%s\">\n",
		 name, name);

  tree = xml_find_child (doc->root, "info");

  schema_render_inputs (p, b, proj_fields, fields, tree);

  buffer_puts (b, " <input type=\"submit\" value=\"Update\"/>\n"
		   "</form>\n");

  render_acceptable_html (vr);

  return render_footer_send (vr);
}

static int
proj_editsub_serve (VirguleReq *vr)
{
  pool *p = vr->r->pool;
  Db *db = vr->db;
  table *args;
  const char *date;
  char *db_key;
  xmlDoc *doc;
  xmlNode *tree;
  int status;
  const char *fields[] = { "url", "fmurl", "license", "notes", NULL };
  const char *name;

  args = get_args_table (vr);
  name = ap_table_get (args, "name");

  db_key = ap_psprintf (p, "proj/%s/info.xml", name);

  doc = db_xml_get (p, vr->db, db_key);
  if (doc == NULL)
    return send_error_page (vr,
			    "Project not found",
			    "Project <tt>%s</tt> was not found.", name);

  if (!proj_ok_to_edit (vr, doc))
    return send_error_page (vr, "Not authorized",
			    "You are not authorized to edit project %s. You have to either be certified to Apprentice level or higher, or be the creator of the project page and not have anyone else edit the page before you.", name);
 
  tree = xml_find_child (doc->root, "info");
  date = iso_now (p);

  schema_put_fields (p, proj_fields, fields, tree, args);

  xml_put_prop (p, tree, "mdate", date);
  xml_put_prop (p, tree, "lastmodby", vr->u);

  if (req_ok_to_post (vr))
    xml_put_prop (p, tree, "locked", "yes");

  status = db_xml_put (p, db, db_key, doc);
  if (status)
    return send_error_page (vr,
			    "Error storing project page",
			    "There was an error storing the project page. This means there's something wrong with the site.");

  add_recent (p, db, "recent/proj-m.xml", name, 50);

  return send_error_page (vr,
			  "Project updated",
			  "Project %s updated.\n",
			  render_proj_name (vr, name));
}

static int
proj_relsub_serve (VirguleReq *vr)
{
  pool *p = vr->r->pool;
  Db *db = vr->db;
  table *args;
  const char *date;
  char *db_key;
  xmlDoc *doc;
  xmlNode *tree;
  int status;
  const char *name;
  const char *type;
  const char *values[3];

  args = get_args_table (vr);
  name = ap_table_get (args, "name");

  db_key = ap_psprintf (p, "proj/%s/info.xml", name);

  doc = db_xml_get (p, db, db_key);
  if (doc == NULL)
    return send_error_page (vr,
			    "Project not found",
			    "Project <tt>%s</tt> was not found.", name);

  if (!proj_ok_to_edit (vr, doc))
    return send_error_page (vr, "Not authorized",
			    "You are not authorized to edit project %s. You have to either be certified to Apprentice level or higher, or be the creator of the project page and not have anyone else edit the page before you.", name);
 
  type = ap_table_get (args, "type");

  /* some this stuffing should probably be done by schema */
  values[0] = name;
  values[1] = vr->u;
  values[2] = type;

  db_relation_put (p, db, &staff_db_rel, values);

  return send_error_page (vr, "Relationship updated",
			  "The update of the relationship between person %s and project %s (type %s) is completed. Have a great day!",
			  vr->u, render_proj_name (vr, name), type);
}

int
proj_serve (VirguleReq *vr)
{
  const char *p;
  if ((p = match_prefix (vr->uri, "/proj/")) == NULL)
    return DECLINED;

  if (!strcmp (p, "new.html"))
    return proj_new_serve (vr);

  if (!strcmp (p, "newsub.html"))
    return proj_newsub_serve (vr);

  if (!strcmp (p, "edit.html"))
    return proj_edit_serve (vr);
 
  if (!strcmp (p, "editsub.html"))
    return proj_editsub_serve (vr);

  if (!strcmp (p, "relsub.html"))
    return proj_relsub_serve (vr);

 if (!strcmp (p, ""))
    return proj_index_serve (vr);

  return proj_proj_serve (vr, p);
}
