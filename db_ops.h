/* This file contains a number of useful operations based on the db
   layer below, such as keeping a list of recent updates, and so
   on. As I develop advogato, it's likely that I'll put in schemas for
   relations, ontology, indexing, and some other things. */


int
add_recent (pool *p, Db *db, const char *key, const char *val, int n_max);


/* Relations */

typedef struct _DbRelation DbRelation;
typedef struct _DbField DbField;
typedef struct _DbSelect DbSelect;

typedef enum {
  DB_FIELD_INDEX = 1,
  DB_FIELD_UNIQUE = 2
} DbFieldFlags;


typedef enum {
  DB_REL_DUMMY
  /* I'm sure there is stuff in here relevant to date handling */
} DbRelFlags;

struct _DbField {
  char *name;
  char *prefix;
  DbFieldFlags flags;
};

struct _DbRelation {
  char *name;
  int n_fields;
  DbField *fields;
  DbRelFlags flags;
};

int
db_relation_put (pool *p, Db *db, const DbRelation *rel, const char **values);

int
db_relation_get (pool *p, Db *db, const DbRelation *rel, char **values);

DbSelect *
db_relation_select (pool *p, Db *db, const DbRelation *rel, char **values);

int
db_relation_select_next (DbSelect *dbs, char **values);
