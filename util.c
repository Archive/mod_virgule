#include <ctype.h>
#include "httpd.h"

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "wiki.h"
#include "util.h"

static const char basis_64[] = 
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; 

char *
b64enc (pool *p, const char *data, int size)
{
  char *result;
  int result_blocks;
  int i;

  result_blocks = ((size + 2) / 3);
  result = ap_pcalloc (p, 4 * result_blocks + 1);
  for (i = 0; i < result_blocks; i++)
    {
      int rem = size - i * 3;
      int d1 = data[i * 3] & 0xff;
      int d2 = rem > 1 ? data[i * 3 + 1] & 0xff : 0;
      int d3 = rem > 2 ? data[i * 3 + 2] & 0xff : 0;
      result[i * 4] = basis_64[(d1 >> 2) & 0x3f];
      result[i * 4 + 1] = basis_64[((d1 & 0x03) << 4) | ((d2 & 0xf0) >> 4)];
      result[i * 4 + 2] = rem > 1 ? basis_64[((d2 & 0x0f) << 2) |
					    ((d3 & 0xc0) >> 6)] : '=';
      result[i * 4 + 3] = rem > 2 ? basis_64[d3 & 0x3f] : '=';
    }
  return result;
}

/**
 * rand_cookie: Create a new, random cookie.
 * @p: pool in which to allocate.
 *
 * Creates a new, base-64 encoded random cookie. The cookie has 120
 * bits of entropy, which should be enough for even the paranoid, and
 * also aligns nicely in base64.
 *
 * Return value: The random cookie.
 **/
char *
rand_cookie (pool *p)
{
  int fd;
  char buf[15];
  int bytes_read;

  fd = ap_popenf (p, "/dev/random", O_RDONLY, 0664);
  if (fd == -1)
    return NULL;
  bytes_read = read (fd, buf, sizeof(buf));
  ap_pclosef (p, fd);
  if (bytes_read < sizeof(buf))
    {
      return NULL;
    }
  return b64enc (p, buf, sizeof(buf));
}

const char *
match_prefix (const char *url, const char *prefix)
{
  int len;
  len = strlen (prefix);
  if (!strncmp (url, prefix, len))
    return url + len;
  else
    return NULL;
}

/**
 * escape_noniso_char: Provide a plausible replacement for non-iso characters.
 * @c: Character to escape.
 *
 * If a character is not an ISO printable, provide a replacement. Otherwise,
 * return NULL.
 *
 * I was very tempted to name this routine "defenestrate_char", as a pun
 * on "escape windows char". Cooler heads prevailed.
 *
 * Reference: http://czyborra.com/charsets/codepages.html#CP1252
 *
 * Return value: Replacement string.
 **/
static const char *
escape_noniso_char (char c)
{
  int u = c & 0xff;

  if ((u >= 0x20 && u <= 0x80) ||
      u >= 0xa0)
    return NULL;
  switch (u)
    {
    case 0x80:
      return "[Euro]";
    case 0x82:
      return ",";
    case 0x83:
      return "f";
    case 0x84:
      return ",,";
    case 0x85:
      return "...";
    case 0x86:
      return "[dagger]";
    case 0x87:
      return "[dbldagger]";
    case 0x88:
      return "^";
    case 0x89:
      return "%0";
    case 0x8A:
      return "S";
    case 0x8B:
      return "&lt;";
    case 0x8C:
      return "OE";
    case 0x8E:
      return "Z";
    case 0x91:
      return "`";
    case 0x92:
      return "'";
    case 0x93:
      return "``";
    case 0x94:
      return "''";
    case 0x95:
      return "*";
    case 0x96:
      return "-";
    case 0x97:
      return "--";
    case 0x98:
      return "~";
    case 0x99:
      return "[TM]";
    case 0x9A:
      return "s";
    case 0x9B:
      return "&gt;";
    case 0x9C:
      return "oe";
    case 0x9E:
      return "z";
    case 0x9F:
      return "Y";
    default:
      /* Unrecognized, just toss. */
      return "";
    }
}

static void
nice_text_cat (char *buf, int *p_j, const char *src, int size)
{
  if (buf) memcpy (buf + *p_j, src, size);
  *p_j += size;
}

static int
nice_text_helper (const char *raw, char *buf)
{
  int i;
  int j;
  int nl_state = 0;
  const char *replacement;

  j = 0;
  for (i = 0; raw[i]; i++)
    {
      char c = raw[i];

      if (c == '\n')
	{
	  nice_text_cat (buf, &j, "\n", 1);

	  if (nl_state == 3)
	    nl_state = 1;
	  else if (nl_state == 1)
	    nl_state = 2;
	}
      else if (c != '\r')
	{
	  if (nl_state == 2)
	    nice_text_cat (buf, &j, "<p> ", 4);
	  nl_state = 3;

	  if (c == '&')
	    nice_text_cat (buf, &j, "&amp;", 5);
	  else if (c == '<')
	    nice_text_cat (buf, &j, "&lt;", 4);
	  else if (c == '>')
	    nice_text_cat (buf, &j, "&gt;", 4);
	  else if ((replacement = escape_noniso_char (c)) != NULL)
	    nice_text_cat (buf, &j, replacement, strlen (replacement));
	  else
	    {
	      if (buf) buf[j] = c;
	      j++;
	    }
	}
    }
  return j;
}

/**
 * nice_text: Convert raw text into nice HTML.
 * @raw: Raw text.
 *
 * Return value: HTML formatted text.
 **/
char *
nice_text (pool *p, const char *raw)
{
  char *result;
  int size;

  if (raw == NULL)
    return NULL;
  size = nice_text_helper (raw, NULL);
  result = ap_palloc (p, size + 1);
  nice_text_helper (raw, result);
  result[size] = '\0';
  return result;
}

static char *
nice_person_link (VirguleReq *vr, const char *name)
{
  pool *p = vr->r->pool;
  int i;

  for (i = 0; name[i]; i++)
    if (!isalnum (name[i]))
      return ap_psprintf (p, "&lt;person&gt;%s&lt;/person&gt;", name);
  return ap_psprintf (p, "<a href=\"%s/person/%s/\">%s</a>",
		      vr->prefix, name, name);
}

static char *
nice_proj_link (VirguleReq *vr, const char *proj)
{
  pool *p = vr->r->pool;

  if (strchr (proj, '/') || proj[0] == '.' || strlen (proj) > 30)
    return ap_psprintf (p, "&lt;proj&gt;%s&lt;/proj&gt;", proj);
  return ap_psprintf (p, "<a href=\"%s/proj/%s/\">%s</a>",
                      vr->prefix, ap_escape_uri (p, proj), proj);
}

typedef struct _AllowedTag AllowedTag;

struct _AllowedTag {
  char *tagname;
  int empty;
  char *(*handler) (VirguleReq *vr, const char *str);
};

static AllowedTag allowed_tags[] = {
  { "a", 0 },
  { "i", 0 },
  { "b", 0 },
  { "tt", 0 },
  { "ol", 0 },
  { "ul", 0 },
  { "li", 1 },
  { "p", 1 },
  { "div", 0 },
  { "strong", 0 },
  { "em", 0 },
  { "cite", 0 },
  { "blockquote", 0 },
  { "br", 1 },
  { "pre", 0 },
  { "person", 0, nice_person_link },
  { "proj", 0, nice_proj_link },
  { "project", 0, nice_proj_link },
  { "wiki", 0, wiki_link }
};

/**
 * match_tag: Match the html source against the tag.
 * @src: Pointer to html source, right after the '<'.
 * @tag: Tag name, in lower case.
 *
 * Return value: pointer to next token after tag if matched, NULL
 * otherwise.
 **/
static const char *
match_tag (const char *src, const char *tag)
{
  int i;
  char c;

  for (i = 0; isalpha (c = src[i]); i++)
    if (tolower (c) != tag[i])
      return NULL;
  if (tag[i] != 0)
    return NULL;
  while (isspace (src[i])) i++;
  return src + i;
}

static const char *
find_end_tag (const char *str, const char *tag, const char **after)
{
  int i;

  for (i = 0; str[i]; i++)
    {
      if (str[i] == '<')
	{
	  const char *ptr;

	  if (str[i + 1] != '/')
	    return NULL;

	  /* Allow </> close tag syntax. */
	  if (str[i + 2] == '>')
	    ptr = str + i + 2;
	  else
	    ptr = match_tag (str + i + 2, tag);

	  if (ptr == NULL) return NULL;
	  if (ptr[0] != '>')
	    return NULL;
	  *after = ptr + 1;
	  return str + i;
	}
    }
  return NULL;
}

/**
 * nice_htext: Convert raw html'ish text into nice HTML.
 * @raw: Raw text.
 * @p_error: Where error message is to be stored.
 *
 * Return value: HTML formatted text.
 **/
char *
nice_htext (VirguleReq *vr, const char *raw, char **p_error)
{
  pool *p = vr->r->pool;
  Buffer *b = buffer_new (p);
  array_header *tag_stack;
  int i, end;
  char c;
  int nl_state = 0;
  const int n_allowed_tags = sizeof (allowed_tags) / sizeof (allowed_tags[0]);
  int in_quote = 0;

  *p_error = NULL;
#if 0
  /* revert to old nicetext behavior */
  return nice_text (p, raw);
#endif
  tag_stack = ap_make_array (p, 16, sizeof (char *));
  for (i = 0; raw[i]; i = end)
    {
      for (end = i;
	   (c = raw[end]) &&
	     c != '\n' && c != '&' && c != '<' && c != '>' && !(c & 0x80);
	   end++);
      if (end > i + 1 || raw[i] != '\r')
	{
	  if (nl_state == 2)
	    buffer_puts (b, "<p> ");
	  nl_state = 3;
	}
      if (end > i)
	buffer_write (b, raw + i, end - i);
      i = end;
      if (c == '&')
	{
	  end++;
	  if (raw[end] == '#')
	    {
	      /* numeric entity */
	      if (isdigit (raw[end + 1]))
		{
		  /* decimal character reference */
		  end += 2;
		  while (isdigit (raw[end])) end++;
		}
#if 0
	      /* apparently, the &#x123; syntax is not valid HTML,
		 even though it is XML. */
	      else if (raw[end + 1] == 'x')
		{
		  /* hexadecimal character reference */
		  if (isxdigit (raw[end + 2]))
		    {
		      end += 3;
		      while (isxdigit (raw[end])) end++;
		    }
		}
#endif
	    }
	  else
	    {
	      /* entity reference */
	      while (isalpha (raw[end])) end++;
	    }
	  if (end > i + 1 && raw[end] == ';')
	    {	
	      end++;
	      buffer_write (b, raw + i, end - i);
	      continue;
	    }
	  end = i + 1;
	  buffer_puts (b, "&amp;");
	}
      else if (c == '<')
	{
	  int j;
	  const char *tail = NULL; /* to avoid uninitialized warning */
	  end++;
	  if (raw[end] == '/')
	    {
	      char *tos;
	      int tos_idx;

	      /* just skip closing tag for empty elements */
	      for (j = 0; j < n_allowed_tags; j++)
		if (allowed_tags[j].empty &&
		    (tail = match_tag (raw + end + 1, allowed_tags[j].tagname)) != NULL)
		    break;

	      if (j < n_allowed_tags)
		{
		  end = tail - raw + 1;
		  continue;
		}

	      /* pop tag stack if closing tag matches */
	      tos_idx = tag_stack->nelts - 1;
	      if (tos_idx >= 0)
		{
		  tos = ((char **)(tag_stack->elts))[tos_idx];

		  /* Allow </> syntax to close tags. */
		  if (raw[end + 1] == '>')
		    tail = raw + end + 1;
		  else
		    tail = match_tag (raw + end + 1, tos);

		  if (tail != NULL && *tail == '>')
		    {
		      buffer_printf (b, "</%s>", tos);
		      tag_stack->nelts--;
		      end = tail - raw + 1;
		      continue;
		    }
		}
	      j = n_allowed_tags;
	    }
	  else
	    {
	      for (j = 0; j < n_allowed_tags; j++)
		{
		  tail = match_tag (raw + end, allowed_tags[j].tagname);
		  if (tail != NULL)
		    break;
		}
	    }
	  if (j < n_allowed_tags)
	    {
	      /* todo: handle quotes */
	      while ((c = raw[end]) && c != '>')
		{
		  if (raw[end] == '"')
		    in_quote = !in_quote;
		  end++;
		}
	      if (c == '>')
		{

		  end++;
		  if (allowed_tags[j].handler != NULL)
		    {
		      char *body;
		      int body_size;
		      const char *body_end;
		      const char *after;

		      body_end = find_end_tag (raw + end,
					       allowed_tags[j].tagname,
					       &after);
		      if (body_end != NULL)
			{
			  body_size = body_end - (raw + end);
			  body = ap_palloc (p, body_size + 1);
			  memcpy (body, raw + end, body_size);
			  body[body_size] = 0;
#if 1
			  buffer_puts (b, allowed_tags[j].handler (vr, body));
#else
			  buffer_printf (b, "[body = %s, %d]", body, body_size);
#endif
			  end = after - raw;
			  continue;
			}

		      else
			{
#if 0
			  buffer_printf (b, "[body_end = NULL]");
#endif
			}
		    }
		  else
		    {
		      if (in_quote)
			{
			  buffer_write (b, raw + i, end - i - 2);
			  buffer_puts (b, "\">");
			  *p_error = "Unterminated quote in tag";
			}
		      else
			buffer_write (b, raw + i, end - i);
		      if (!allowed_tags[j].empty)
			{
			  char **p_stack;
			  
			  p_stack = (char **)ap_push_array (tag_stack);
			  *p_stack = allowed_tags[j].tagname;
			}
		      continue;
		    }
		}
	      end = i + 1;
	    }
	  /* tag not matched, escape the html */
	  buffer_puts (b, "&lt;");
	}
      else if (c == '>')
	{
	  end++;
	  buffer_puts (b, "&gt;");
	}
      else if (c == '\n')
	{
	  end++;
	  buffer_puts (b, "\n");

	  if (nl_state == 3)
	    nl_state = 1;
	  else if (nl_state == 1)
	    nl_state = 2;
	}
      else if (c != 0)
	{
	  const char *replacement;
	  end++;
	  replacement = escape_noniso_char (c);
	  if (replacement == NULL)
	    buffer_write (b, raw + i, end - i);
	  else
	    buffer_puts (b, replacement);
	}
    }

  /* close all open tags */
  while (tag_stack->nelts)
    {
      int tos_idx;
      char *tos;

      tos_idx = --tag_stack->nelts;
      tos = ((char **)(tag_stack->elts))[tos_idx];
      buffer_printf (b, "</%s>", tos);
      if (*p_error == NULL)
	*p_error = ap_psprintf (p, "Unclosed tag %s", tos);
    }

  return buffer_extract (b);
}

/**
 * iso_now: Current date/time in iso format.
 * Return value: Time in "YY-MM-DD hh:mm:ss" format.
 **/
char *
iso_now (pool *p)
{
  return ap_ht_time (p, time (NULL), "%Y-%m-%d %H:%M:%S", 0);
}

/**
 * str_subst: Simple search-and-replace string substitution.
 * @p: The pool.
 * @str: Original string.
 * @pattern: Pattern to search for.
 * @repl: Replacement string.
 *
 * Replaces all occurrences of @pattern in @str with @repl.
 *
 * Return value: The resulting string.
 **/
char *
str_subst (pool *p, const char *str, const char *pattern, const char *repl)
{
  int size, idx;
  int i, j;
  int repl_len;
  char *result;

  if (pattern[0] == 0)
    return NULL;

  repl_len = strlen (repl);

  size = 0;
  for (i = 0; str[i]; i++)
    {
      for (j = 0; pattern[j]; j++)
	if (str[i + j] != pattern[j])
	  break;
      if (pattern[j] == 0)
	{
	  size += repl_len;
	  i += j - 1;
	}
      else
	size++;
    }
  result = ap_palloc (p, size + 1);

  idx = 0;
  for (i = 0; str[i]; i++)
    {
      for (j = 0; pattern[j]; j++)
	if (str[i + j] != pattern[j])
	  break;
      if (pattern[j] == 0)
	{
	  memcpy (result + idx, repl, repl_len);
	  idx += repl_len;
	  i += j - 1;
	}
      else
	result[idx++] = str[i];
    }
  result[idx] = 0;
  return result;
}

/**
 * escape_uri_arg: Escape string in form suitable for URI argument.
 * @p: The pool.
 * @str: The original string.
 *
 * The same as ap_escape_uri except that '+' is also escaped to %2b.
 *
 * Return value: the escaped string.
 **/
char *
escape_uri_arg (pool *p, const char *str)
{
  char *tmp = ap_escape_uri (p, str);
  return str_subst (p, tmp, "+", "%2b");
}
