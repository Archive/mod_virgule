/* This file contains features to export site contents through
   Netscape's RSS.

   Netscape has some documentation up at:
   http://my.netscape.com/publish/help/mnn20/quickstart.html

   Module written by Martijn van Beers.

*/

#include "httpd.h"

#include <tree.h>
#include <xmlmemory.h>

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "util.h"
#include "db_xml.h"
#include "xml_util.h"

#include "rss_export.h"

static void
rss_render_from_xml (pool *p, int art_num, xmlDoc *doc, xmlNodePtr tree)
{
  xmlNode *root = doc->root;
  xmlNodePtr subtree;
  char *title;
  char *description;
  char *link;

  title = xml_find_child_string (root, "title", "(no title)");
  link = ap_psprintf (p, "http://advogato.org/article/%d.html", art_num);
  description = xml_find_child_string (root, "lead", "(no description)");
  

  subtree = xmlNewChild (tree, NULL, "title", title);
  subtree = xmlNewChild (tree, NULL, "link", link);
  subtree = xmlNewChild (tree, NULL, "description", description);

}

static void
rss_render (VirguleReq *vr, int art_num, xmlNodePtr tree)
{
  pool *p = vr->r->pool;
  char *key;
  xmlDoc *doc;
  xmlNodePtr subtree;

  key = ap_psprintf (p, "articles/_%d/article.xml", art_num);
  doc = db_xml_get (p, vr->db, key);
  if (doc != NULL)
    {
      subtree = xmlNewChild (tree, NULL, "item", NULL);
      rss_render_from_xml (p, art_num, doc, subtree);
    }
}

static int
rss_index_serve (VirguleReq *vr)
{
  Buffer *b = vr->b;
  xmlDocPtr doc;
  xmlNodePtr tree, subtree;
  int art_num;
  int n_arts;
  xmlChar *mem;
  int size;

  doc = xmlNewDoc ("1.0");
  xmlCreateIntSubset (doc, "rss",
		      "-//Netscape Communications//DTD RSS 0.91//EN",
		      "http://my.netscape.com/publish/formats/rss-0.91.dtd");
  doc->root = xmlNewDocNode (doc, NULL, "rss", NULL);
  xmlSetProp (doc->root, "version", "0.91");
  tree = xmlNewChild (doc->root, NULL, "channel", NULL);
  subtree = xmlNewChild (tree, NULL, "title", "Advogato");
  subtree = xmlNewChild (tree, NULL,
			 "link", "http://advogato.org/article/");
  subtree = xmlNewChild (tree, NULL,
			 "description", "Recent Advogato articles");
  subtree = xmlNewChild (tree, NULL, "language", "en-us");

  art_num = db_dir_max (vr->db, "articles");

  for (n_arts = 0; n_arts < 10 && art_num >= 0; n_arts++)
    {
      rss_render (vr, art_num, tree);
      art_num--;
    }

  xmlDocDumpMemory (doc, &mem, &size);
  buffer_write (b, mem, size);
  xmlFree (mem);
  xmlFreeDoc (doc);
  return send_response (vr);
}

int
rss_serve (VirguleReq *vr)
{
   const char *p;
   if ((p = match_prefix (vr->r->uri, "/rss/")) == NULL)
     return DECLINED;

   if (!strcmp (p, "articles.xml"))
     return rss_index_serve (vr);

   return DECLINED;
}
