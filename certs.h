typedef enum {
  CERT_LEVEL_NONE,
  CERT_LEVEL_APPRENTICE,
  CERT_LEVEL_JOURNEYER,
  CERT_LEVEL_MASTER
} CertLevel;

extern int cert_level_n;

#define CERT_LEVEL_MAX CERT_LEVEL_MASTER

typedef enum {
  CERT_STYLE_SMALL,
  CERT_STYLE_LARGE
} CertStyle;

CertLevel
cert_level_from_name (const char *name);

char *
cert_level_to_name (CertLevel level);

CertLevel
cert_get (pool *p, Db *db, const char *issuer, const char *subject);

int
cert_set (pool *p, Db *db, const char *issuer, const char *subject, CertLevel level);

void
render_cert_level_begin (VirguleReq *vr, const char *user, CertStyle cs);

void
render_cert_level_end (VirguleReq *vr, CertStyle cs);
