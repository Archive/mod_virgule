/* This module serves xml files into HTML, using a simple hardcoded
   stylesheet. */

#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"
#include "ap_config.h"

#include <tree.h>
#include <parser.h>

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "style.h"
#include "db_xml.h"
#include "xml_util.h"
#include "article.h"
#include "certs.h"
#include "diary.h"
#include "proj.h"
#include "wiki.h"

#include "site.h"

typedef struct _RenderCtx RenderCtx;

struct _RenderCtx {
  int in_body;
  VirguleReq *vr;
  char *title;
};

static void site_render (RenderCtx *ctx, xmlNode *node);


static void
site_render_children (RenderCtx *ctx, xmlNode *node)
{
  xmlNode *child;

  for (child = node->childs; child != NULL; child = child->next)
    site_render (ctx, child);
}

/**
 * site_render_person_link: Render a link to a person.
 * @vr: #VirguleReq context.
 * @name: The person's name.
 *
 * Renders the link to the person, outputting to @vr's buffer.
 **/
static void
site_render_person_link (VirguleReq *vr, const char *name)
{
  buffer_printf (vr->b, "<a href=\"%s/person/%s/\" style=\"text-decoration: none\">%s</a>\n",
		 vr->prefix, name, name);
}

static void
site_render_recent_acct (VirguleReq *vr, const char *list, int n_max)
{
  pool *p = vr->r->pool;
  char *key;
  xmlDoc *doc;
  xmlNode *root, *tree;
  int n;
  char *bk = "";

  key = ap_psprintf (p, "recent/%s.xml", list);
  doc = db_xml_get (p, vr->db, key);
  if (doc == NULL)
    return;
  root = doc->root;
  n = 0;
  for (tree = root->last; tree != NULL && n < n_max; tree = tree->prev)
    {
      char *name = xml_get_string_contents (tree);
      char *date = xml_get_prop (p, tree, "date");
      buffer_puts (vr->b, bk);
      bk = "<spacer type=\"vertical\" size=\"2\">";
      render_cert_level_begin (vr, name, CERT_STYLE_SMALL);
      buffer_printf (vr->b, " %s ", render_date (vr, date));
      site_render_person_link (vr, name);
      render_cert_level_end (vr, CERT_STYLE_SMALL);
      n++;
    }
}

static void
site_render_recent_changelog (VirguleReq *vr, int n_max)
{
  pool *p = vr->r->pool;
  char *key;
  xmlDoc *doc;
  xmlNode *root, *tree;
  int n;
  char *bk = "";

  key = ap_psprintf (p, "recent/%s.xml", "diary");
  doc = db_xml_get (p, vr->db, key);
  if (doc == NULL)
    return;
  root = doc->root;
  n = 0;
  for (tree = root->last; tree != NULL && n < n_max; tree = tree->prev)
    {
      char *name = xml_get_string_contents (tree);
      char *date = xml_get_prop (p, tree, "date");
      buffer_puts (vr->b, bk);
      bk = "<spacer type=\"vertical\" size=\"2\">";
      render_cert_level_begin (vr, name, CERT_STYLE_SMALL);
      buffer_printf (vr->b, " %s ", render_date (vr, date));
      site_render_person_link (vr, name);
      render_cert_level_end (vr, CERT_STYLE_SMALL);
      /* Render the most recent entry in the diary for this user... */
      diary_latest_render (vr, name);
      n++;
    }
}

static void
site_render_recent_proj (VirguleReq *vr, const char *list, int n_max)
{
  pool *p = vr->r->pool;
  char *key;
  xmlDoc *doc;
  xmlNode *root, *tree;
  int n;
  char *bk = "";

  key = ap_psprintf (p, "recent/%s.xml", list);
  doc = db_xml_get (p, vr->db, key);
  if (doc == NULL)
    return;
  root = doc->root;
  n = 0;
  for (tree = root->last; tree != NULL && n < n_max; tree = tree->prev)
    {
      char *name = xml_get_string_contents (tree);
      char *date = xml_get_prop (p, tree, "date");
      buffer_puts (vr->b, bk);
      bk = "<br><spacer type=\"vertical\" size=\"2\">";
      buffer_printf (vr->b, " %s %s\n",
		     render_date (vr, date), render_proj_name (vr, name));
      n++;
    }
}

static void
site_render (RenderCtx *ctx, xmlNode *node)
{
  VirguleReq *vr = ctx->vr;
  pool *p = vr->r->pool;
  Buffer *b = vr->b;
  if (node->type == XML_TEXT_NODE)
    {
      buffer_puts (b, node->content);
    }
  else if (node->type == XML_ELEMENT_NODE)
    {
      if (!strcmp (node->name, "page"))
	{
	  site_render_children (ctx, node);
	}
      else if (!strcmp (node->name, "title"))
	{
	  /* skip */
	}
      else if (!strcmp (node->name, "thetitle"))
	{
	  buffer_puts (b, ctx->title);
	}
      else if (!strcmp (node->name, "br"))
	{
	  buffer_puts (b, "<br>\n");
	}
      else if (!strcmp (node->name, "dt"))
	{
	  buffer_puts (b, "<dt>\n");
	  site_render_children (ctx, node);
	}
      else if (!strcmp (node->name, "recent"))
	{
	  char *list, *nmax_str;
	  int nmax;

	  list = xml_get_prop (p, node, "list");
	  nmax_str = xml_get_prop (p, node, "nmax");
	  if (nmax_str)
	    nmax = atoi (nmax_str);
	  else
	    nmax = 10;
	  site_render_recent_acct (vr, list, nmax);
	}
      else if (!strcmp (node->name, "recentlog"))
	{
	  char *nmax_str;
	  int nmax;

	  nmax_str = xml_get_prop (p, node, "nmax");
	  if (nmax_str)
	    nmax = atoi (nmax_str);
	  else
	    nmax = 10;
	  site_render_recent_changelog (vr, nmax);
	}
      else if (!strcmp (node->name, "recentproj"))
	{
	  char *list, *nmax_str;
	  int nmax;

	  list = xml_get_prop (p, node, "list");
	  nmax_str = xml_get_prop (p, node, "nmax");
	  if (nmax_str)
	    nmax = atoi (nmax_str);
	  else
	    nmax = 10;
	  site_render_recent_proj (vr, list, nmax);
	}
      else if (!strcmp (node->name, "articles"))
	{
	  char *list, *nmax_str;
	  int nmax;

	  list = xml_get_prop (p, node, "list");
	  nmax_str = xml_get_prop (p, node, "nmax");
	  if (nmax_str)
	    nmax = atoi (nmax_str);
	  else
	    nmax = 10;
	  article_recent_render (vr, nmax, -1);
	}
      else if (!strcmp (node->name, "sitemap"))
	{
	  render_sitemap (vr, 0);
	}
#if 0
      else if (!strcmp (node->name, "wiki"))
	{
	  buffer_puts (b, wiki_link (vr, xml_get_string_contents (node)));
	}
#endif
      else
	{
	  xmlAttr *a;
	  /* default: just pass through */
	  buffer_append (b, "<", node->name, NULL);
	  for (a = node->properties; a != NULL; a = a->next)
	    {
	      buffer_append (b, " ", a->name, "=\"", NULL);
	      site_render (ctx, a->val);
	      buffer_puts (b, "\"");
	    }
	  buffer_puts (b, ">\n");
	  if (!strcmp (node->name, "td"))
	    render_table_open (vr);
	  site_render_children (ctx, node);
	  if (!strcmp (node->name, "td"))
	    render_table_close (vr);
	  if (strcmp (node->name, "input") &&
	      strcmp (node->name, "spacer"))
	    buffer_append (b, "</", node->name, ">\n", NULL);
	}
    }
}

static int
site_render_page (VirguleReq *vr, xmlNode *node)
{
  RenderCtx ctx;
  xmlNode *title_node;
  char *title;
  char *raw;

  ctx.vr = vr;

  title_node = xml_find_child (node, "title");
  if (title_node == NULL)
    title = "(no title)";
  else
    title = xml_get_string_contents (title_node);

  ctx.title = title;
  raw = xml_get_prop (vr->r->pool, node, "raw");
  if (raw)
    render_header_raw (vr, title);
  else
    render_header (vr, title);
  site_render (&ctx, node);

  return render_footer_send (vr);
}

/* Return Apache error code. */
int
site_serve (VirguleReq *vr)
{
  request_rec *r = vr->r;
  Buffer *b = vr->b;
  Db *db = vr->db;
  char *uri = vr->uri;
  char *key;
  char *val;
  int val_size;
  int ix;
  char *content_type = NULL;
  int is_xml = 0;
  int len;
  int return_code;

  len = strlen (uri);
  if (len > 0 && uri[len - 1] == '/')
    uri = ap_pstrcat (r->pool, uri, "index.html", NULL);

#if 0
  r->content_type = "text/plain";
  buffer_puts (b, uri);
  return send_response (vr);
#endif

  for (ix = strlen (uri) - 1; ix >= 0; ix--)
    if (uri[ix] == '.' || uri[ix] == '/')
      break;
  if (ix >= 0 && uri[ix] == '.')
    {
      /* there is an extension */
      if (!strcmp (uri + ix + 1, "html"))
	{
	  content_type = "text/html";

	  is_xml = 1;
	  uri = ap_pstrcat (r->pool, ap_pstrndup (r->pool, uri, ix + 1),
			    "xml", NULL);
	}
    }

  key = ap_pstrcat (r->pool, "/site", uri, NULL);

  /* The ordering (doing content type first, dir detection second) is
     not pleasing. */
  if (db_is_dir (db, key))
    {
      ap_table_add (r->headers_out, "Location",
		    ap_make_full_path (r->pool, vr->uri, ""));
      return REDIRECT;
    }

  if (content_type == NULL)
    return DECLINED;

  r->content_type = content_type;

  val = db_get (db, key, &val_size);

  if (val == NULL)
    return DECLINED;

  if (is_xml)
    {
      xmlDoc *doc;

      doc = xmlParseMemory (val, val_size);
      if (doc == NULL)
	{
	  buffer_puts (b, "xml parsing error\n");
	}
      else
	{
	  xmlNode *root;

	  root = doc->root;
	  return_code = site_render_page (vr, root);
	  xmlFreeDoc (doc);
	  return return_code;
	}
    }
  else
    {
      buffer_write (b, val, val_size);
    }

  return send_response (vr);
}
