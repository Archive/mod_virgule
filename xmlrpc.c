#include "httpd.h"
#include "http_log.h"

#include <tree.h>
#include <parser.h>
#include <xmlmemory.h>

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "auth.h"

#include "xmlrpc.h"
#include "xmlrpc-methods.h"


/* Create <value>s */
static xmlNode *
xmlrpc_value_int (xmlNode *node, long i)
{
  char buf[12];
  ap_snprintf(buf, 12, "%ld", i);

  node = xmlNewChild (node, NULL, "value", NULL);
  node = xmlNewChild (node, NULL, "int", buf);
  return node;
}

static xmlNode *
xmlrpc_value_string (xmlNode *node, const char *s)
{
  node = xmlNewChild (node, NULL, "value", NULL);
  node = xmlNewChild (node, NULL, "string", s);
  return node;
}

static xmlNode *
xmlrpc_value_struct (xmlNode *node)
{
  node = xmlNewChild (node, NULL, "value", NULL);
  node = xmlNewChild (node, NULL, "struct", NULL);
  return node;
}

static xmlNode *
xmlrpc_struct_add_member (xmlNode *node, const char *name)
{
  node = xmlNewChild (node, NULL, "member", NULL);
  xmlNewChild (node, NULL, "name", name);
  return node;
}


/* Create and send responses */
static xmlNode *
xmlrpc_create_response (VirguleReq *vr)
{
  xmlDoc *r = xmlNewDoc ("1.0");
  r->root = xmlNewDocNode (r, NULL, "methodResponse", NULL);
  return r->root->doc == r ? r->root : NULL;
}

static int
xmlrpc_send_response (VirguleReq *vr, xmlNode *r)
{
  xmlChar *mem;
  int size;

  xmlDocDumpMemory (r->doc, &mem, &size);
  buffer_write (vr->b, mem, size);
  xmlFree (mem);
  xmlFreeDoc (r->doc);

  vr->r->content_type = "text/xml";
  return send_response (vr);
}


/* Create and send a fault response */
int
xmlrpc_fault (VirguleReq *vr, int code, const char *fmt, ...)
{
  xmlNode *resp, *str;
  va_list ap;
  char *msg;

  va_start (ap, fmt);
  msg = ap_pvsprintf (vr->r->pool, fmt, ap);
  va_end (ap);
  
  resp = xmlrpc_create_response (vr);

  str = xmlrpc_value_struct (xmlNewChild (resp, NULL, "fault", NULL));
  xmlrpc_value_int (xmlrpc_struct_add_member (str, "faultCode"), code);
  xmlrpc_value_string (xmlrpc_struct_add_member (str, "faultString"), msg);

  return xmlrpc_send_response (vr, resp);
}


/* Create and send a normal response */
int
xmlrpc_response (VirguleReq *vr, const char *types, ...)
{
  xmlNode *resp;
  xmlNode *container;
  va_list va;
  int i;
  
  if (!strlen (types))
    return xmlrpc_fault (vr, 1, "internal error: must return something!");

  resp = xmlrpc_create_response (vr);
  container = xmlNewChild (xmlNewChild (resp, NULL, "params", NULL),
                           NULL, "param", NULL);

  if (strlen (types) > 1)
    container = xmlNewChild (xmlNewChild (resp, NULL, "array", NULL),
                       NULL, "data", NULL);

  va_start (va, types);
  for (i=0; i<strlen (types); i++)
    {
      switch (types[i])
        {
	case 'i':
          xmlrpc_value_int (container, va_arg (va, int));
          break;

        case 's':
          xmlrpc_value_string (container, va_arg (va, char *));
          break;
          
        default:
          va_end (va);
          return xmlrpc_fault (vr, 1, "internal error: unknown type '%c'",
                               types[i]);
	}
    }
  va_end (va);
  
  return xmlrpc_send_response (vr, resp);
}


/* Authenticate the user */
int
xmlrpc_auth_user (VirguleReq *vr, const char *cookie)
{
  auth_user_with_cookie (vr, cookie);
  if (vr->u == NULL)
    return xmlrpc_fault (vr, 1, "authentication failure");
  
  return OK;
}


/* Extract the parameters from the request */  
int
xmlrpc_unmarshal_params (VirguleReq *vr, xmlNode *params,
			 const char *types, ...)
{
  xmlNode *param;
  va_list va;
  int argc;
  int i;

  /* check that the caller supplied the right number of parameters */
  argc = 0;
  if (params)
    {
      if (strcmp (params->name, "params"))
        return xmlrpc_fault (vr, 1, "expecting <params>, got <%s>",
                             params->name);  

      for (param = params->childs; param; param = param->next)
        {
          if (xmlIsBlankNode (param))
            continue;
          argc++;
        }
      if (argc != strlen (types))
        return xmlrpc_fault (vr, 1, "expecting %d parameters, got %d",
                             strlen (types), argc);
    }
  else
    {
      if (strlen(types))
        return xmlrpc_fault (vr, 1, "expecting %d parameters, got 0",
                             strlen (types));
      return OK;
    }

  /* unmarshal the parameters */
  param = params->childs;
  va_start (va, types);
  for (i=0; i<argc; i++)
    {
      xmlNode *value;
	
      while (xmlIsBlankNode (param))
        param = param->next;

      value = param->childs;
      while (xmlIsBlankNode (value))
        value = value->next;

      value = value->childs;
      while (xmlIsBlankNode (value))
        value = value->next;
	
      switch (types[i])
        {
	case 'i':
          if (!strcmp (value->name, "int") || !strcmp (value->name, "i4"))
	    {
              char *val;
		
              val = xmlNodeListGetString (value->doc, value->childs, 1);
              *va_arg (va, int *) = atoi (val);
              xmlFree (val);
	    }
          else
            {
              va_end (va);
              return xmlrpc_fault (vr, 1,
                                   "param %d: expecting <int>, got <%s>",
                                   i, value->name);
	    }
          break;

        case 's':
          if (!strcmp (value->name, "string"))
            {
              char *val;
		
              val = xmlNodeListGetString (value->doc, value->childs, 1);
              *va_arg (va, char **) = ap_pstrdup (vr->r->pool, val);
              xmlFree (val);
            }
          else
            {
              va_end (va);
              return xmlrpc_fault (vr, 1,
                                   "param %d: expecting <string>, got <%s>",
                                   i, value->name);
	    }
    
          break;
          
	default:
          va_end (va);
          return xmlrpc_fault (vr, 1, "internal error: unknown type '%c'",
                               types[i]);
	}
      param = param->next;
    }
  va_end (va);

  return OK;
}


/* Fob the request off at the appropriate method function */
static int
xmlrpc_unmarshal_request (VirguleReq *vr, xmlNode *xr)
{
  extern xmlrpc_method xmlrpc_method_table[];
  xmlNode *n;
  char *tmp;
  const char *name;
  xmlrpc_method *m;

  /* root element should be a <methodCall> */
  if (strcmp (xr->name, "methodCall"))
    return xmlrpc_fault (vr, 1, "expecting <methodCall>, got <%s>", xr->name);
    
  /* first element of methodCall should be a <methodName> */
  n = xr->childs;
  while (n && xmlIsBlankNode (n))
    n = n->next;

  if (!n || strcmp (n->name, "methodName"))
    return xmlrpc_fault (vr, 1, "expecting <methodName>, got <%s>", n->name);

  tmp = xmlNodeListGetString (n->doc, n->childs, 1);
  name = ap_pstrdup (vr->r->pool, tmp);
  xmlFree (tmp);

  /* second element of methodCall should be a <params> */
  n = n->next;
  while (n && xmlIsBlankNode (n))
    n = n->next;

  /* dispatch the method */
  for (m = xmlrpc_method_table; m->name != NULL; m++)
    {
      if (!strcmp (m->name, name))
        break;
    }
  if (m->name == NULL)
    return xmlrpc_fault (vr, 1, "%s: method not implemented", name);

  return m->func (vr, n);
}


/* Main handler for requests */
int
xmlrpc_serve (VirguleReq *vr)
{
  xmlDoc *request = NULL;
  int ret;
  
  if (strcmp (vr->uri, "/XMLRPC"))
    return DECLINED;

  if (vr->r->method_number != M_POST)
    return METHOD_NOT_ALLOWED;

  request = xmlParseMemory (vr->args, vr->r->read_length);
  if (request == NULL)
    return xmlrpc_fault (vr, 1, "unable to parse request");

  ret = xmlrpc_unmarshal_request (vr, request->root);
  xmlFreeDoc (request);
  return ret;
}
