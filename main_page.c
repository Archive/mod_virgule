/* Generate the Advogato main page. */

#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"
#include "ap_config.h"

#include "buffer.h"
#include "db.h"
#include "req.h"

#include "main_page.h"

int
main_page (VirguleReq *vr)
{
  request_rec *r = vr->r;
  Buffer *b = vr->b;
  r->content_type = "text/html";

  buffer_puts (b, 
	       "<html><head><title>\n"
	       "Advogato\n"
	       "</title>\n"
	       "\n"
	       "<body bgcolor=white>\n"
	       "\n"
	       "<h1>\n"
	       "Advogato\n"
	       "</h1>\n"
	       "\n"
	       "<blockquote>\n"
	       "\n"
	       "<p> This site will soon become Advogato, the free software developer's\n"
	       "advocate. But I really should be working on finishing my thesis rather\n"
	       "than doing webmonkey stuff right now. </p>\n"
	       "\n"
	       "<p> In the meantime, you will likely find the following sites\n"
	       "interesting: </p>\n"
	       "\n"
	       "<ul>\n"
	       "\n"
	       "<li><a href=\"http://lwn.net/\">Linux Weekly News</a>\n"
	       "\n"
	       "<li><a href=\"http://slashdot.org/\">Slashdot</a>\n"
	       "\n"
	       "<li><a href=\"http://www.fsf.org/\">Free Software Foundation</a>\n"
	       "\n"
	       "<li><b>Patents:</b>\n"
	       "\n"
	       "<ul>\n"
	       "\n"
	       "<li><a href=\"http://lpf.ai.mit.edu/\">League for Programming Freedom</a>\n"
	       "\n"
	       "<li><a href=\"http://www.freepatents.org/\">freepatents.org</a>,\n"
	       "a European anti-software patent site\n"
	       "\n"
	       "<li><a href=\"http://womplex.patents.ibm.com/\">IBM Patent Search</a>\n"
	       "\n"
	       "</ul>\n"
	       "\n"
	       "</ul>\n"
	       "\n"
	       "<p> <a href=\"http://www.levien.com/\">Raph Levien</a> </p>\n"
	       "\n"
	       "</blockquote>\n"
	       "\n"
	       "</body></html>\n");
  return send_response (vr);
}
