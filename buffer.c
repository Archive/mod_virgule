#include <stdarg.h>
#include <string.h>

#include "httpd.h"
#include "http_protocol.h"

#include "buffer.h"

/* An abstraction for an appendable buffer. This should be reasonably
efficient in both time and memory usage for most purposes. */

typedef struct _BufferChunk BufferChunk;

struct _BufferChunk {
  BufferChunk *next;
  char *buf;
  int size;
  int size_max;
};

struct _Buffer {
  pool *p;
  int total_size;
  BufferChunk *first_chunk;
  BufferChunk *last_chunk;
};

Buffer *
buffer_new (pool *p)
{
  Buffer *result = ap_palloc (p, sizeof(Buffer));
  BufferChunk *chunk = ap_palloc (p, sizeof(BufferChunk));

  result->p = p;
  result->total_size = 0;
  result->first_chunk = chunk;
  result->last_chunk = chunk;

  chunk->next = NULL;
  chunk->size = 0;
  chunk->size_max = 256;
  chunk->buf = ap_palloc (p, chunk->size_max);

  return result;
}

void
buffer_write (Buffer *b, const char *data, int size)
{
  BufferChunk *last = b->last_chunk;
  int copy_size;
  BufferChunk *new;
  pool *p;

  b->total_size += size;

  copy_size = size;
  if (copy_size + last->size > last->size_max)
    copy_size = last->size_max - last->size;

  memcpy (last->buf + last->size, data, copy_size);
  last->size += copy_size;
  if (copy_size == size)
    return;

  /* Allocate a new chunk. */
  p = b->p;
  new = ap_palloc (p, sizeof(BufferChunk));
  /* append new chunk to linked list */
  new->next = NULL;
  last->next = new;
  b->last_chunk = new;

  new->size = size - copy_size;
  new->size_max = 256;
  while (new->size_max < new->size)
    new->size_max <<= 1;
  new->buf = ap_palloc (p, new->size_max);
  memcpy (new->buf, data + copy_size, size - copy_size);
}

void
buffer_printf (Buffer *b, const char *fmt, ...)
{
  /* It might be worth fiddling with this so that it doesn't copy
     quite so much. */

  char *str;
  va_list ap;
  va_start (ap, fmt);
  str = ap_pvsprintf (b->p, fmt, ap);
  va_end (ap);
  buffer_write (b, str, strlen (str));
}

void
buffer_puts (Buffer *b, const char *str)
{
  buffer_write (b, str, strlen (str));
}

void
buffer_append (Buffer *b, const char *str1, ...)
{
  va_list args;
  char *s;

  buffer_puts (b, str1);
  va_start (args, str1);
  while ((s = va_arg (args, char *)) != NULL)
    buffer_puts (b, s);
  va_end (args);
}

/* Send http header and buffer */
void
buffer_send_response (request_rec *r, Buffer *b)
{
  BufferChunk *chunk;
  ap_set_content_length (r, b->total_size);

  ap_send_http_header (r);
  if (!r->header_only)
    for (chunk = b->first_chunk; chunk != NULL; chunk = chunk->next)
      ap_rwrite (chunk->buf, chunk->size, r);
}

/**
 * buffer_extract: Extract buffer contents to null-terminated string.
 * @b: The buffer.
 *
 * Return value: A null-terminated string corresponding to the buffer.
 **/
char *
buffer_extract (Buffer *b)
{
  char *result;
  BufferChunk *chunk;
  int idx;

  idx = 0;
  result = ap_palloc (b->p, b->total_size + 1);
  for (chunk = b->first_chunk; chunk != NULL; chunk = chunk->next)
    {
      memcpy (result + idx, chunk->buf, chunk->size);
      idx += chunk->size;
    }
  result[idx] = 0;
  return result;
}
