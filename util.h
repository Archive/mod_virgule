char *
b64enc (pool *p, const char *data, int size);

char *
rand_cookie (pool *p);

const char *
match_prefix (const char *url, const char *prefix);

char *
nice_text (pool *p, const char *raw);

char *
nice_htext (VirguleReq *vr, const char *raw, char **p_error);

char *
iso_now (pool *p);

char *
str_subst (pool *p, const char *str, const char *pattern, const char *repl);

char *
escape_uri_arg (pool *p, const char *str);
