#include <string.h>
#include "httpd.h"

#include <tree.h>
#include <xmlmemory.h>

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "db_xml.h"
#include "xml_util.h"
#include "acct_maint.h"
#include "style.h"

#include "certs.h"

static char *cert_level_names[] = {
  "Observer",
  "Apprentice",
  "Journeyer",
  "Master"
};

int cert_level_n = sizeof(cert_level_names) / sizeof(cert_level_names[0]);

CertLevel
cert_level_from_name (const char *name)
{
  int i;

  for (i = 0; i < sizeof(cert_level_names) / sizeof(cert_level_names[0]); i++)
    if (!strcmp (name, cert_level_names[i]))
      return i;
  return CERT_LEVEL_NONE;
}

char *
cert_level_to_name (CertLevel level)
{
  if (level >= 0 && level < cert_level_n)
    return cert_level_names[level];
  return "None";
}

CertLevel
cert_get (pool *p, Db *db, const char *issuer, const char *subject)
{
  char *db_key;
  xmlDoc *profile;
  xmlNode *tree;
  xmlNode *cert;
  CertLevel result = CERT_LEVEL_NONE;

  db_key = acct_dbkey (p, issuer);
  profile = db_xml_get (p, db, db_key);
  tree = xml_find_child (profile->root, "certs");
  if (tree == NULL)
    return CERT_LEVEL_NONE;
  for (cert = tree->childs; cert != NULL; cert = cert->next)
    {
      if (cert->type == XML_ELEMENT_NODE &&
	  !strcmp (cert->name, "cert"))
	{
	  char *cert_subj;

	  cert_subj = xmlGetProp (cert, "subj");
	  if (cert_subj)
	    {
	      if (!strcmp (cert_subj, subject))
		{
		  char *cert_level;

		  cert_level = xmlGetProp (cert, "level");
		  result = cert_level_from_name (cert_level);
		  xmlFree (cert_level);
		  xmlFree (cert_subj);
		  break;
		}
	      xmlFree (cert_subj);
	    }
	}
    }
  db_xml_free (p, db, profile);
  return result;
}

int
cert_set (pool *p, Db *db, const char *issuer, const char *subject, CertLevel level)
{
  char *db_key;
  xmlDoc *profile;
  xmlNode *tree;
  xmlNode *cert;
  int status;

  /* update subject first because it's more likely not to exist. */
  db_key = acct_dbkey (p, subject);
  profile = db_xml_get (p, db, db_key);
  if (profile == NULL)
    return -1;

  tree = xml_ensure_child (profile->root, "certs-in");

  for (cert = tree->childs; cert != NULL; cert = cert->next)
    {
      if (cert->type == XML_ELEMENT_NODE &&
	  !strcmp (cert->name, "cert"))
	{
	  char *cert_issuer;

	  cert_issuer = xmlGetProp (cert, "issuer");
	  if (cert_issuer)
	    {
	      if (!strcmp (cert_issuer, issuer))
		{
		  xmlFree (cert_issuer);
		  break;
		}
	      xmlFree (cert_issuer);
	    }
	}
    }
  if (cert == NULL)
    {
      cert = xmlNewChild (tree, NULL, "cert", NULL);
      xmlSetProp (cert, "issuer", issuer);
    }

  xmlSetProp (cert, "level", cert_level_to_name (level));

  status = db_xml_put (p, db, db_key, profile);
  db_xml_free (p, db, profile);

  /* then, update issuer */
  db_key = acct_dbkey (p, issuer);
  profile = db_xml_get (p, db, db_key);
  if (profile == NULL)
    return -1;
  tree = xml_ensure_child (profile->root, "certs");

  for (cert = tree->childs; cert != NULL; cert = cert->next)
    {
      if (cert->type == XML_ELEMENT_NODE &&
	  !strcmp (cert->name, "cert"))
	{
	  char *cert_subj;

	  cert_subj = xmlGetProp (cert, "subj");
	  if (cert_subj)
	    {
	      if (!strcmp (cert_subj, subject))
		{
		  xmlFree (cert_subj);
		  break;
		}
	      xmlFree (cert_subj);
	    }
	}
    }
  if (cert == NULL)
    {
      cert = xmlNewChild (tree, NULL, "cert", NULL);
      xmlSetProp (cert, "subj", subject);
    }

  xmlSetProp (cert, "level", cert_level_to_name (level));

  status = db_xml_put (p, db, db_key, profile);
  db_xml_free (p, db, profile);

  return status;
}

/**
 * render_cert_level_begin: Begin rendering cert level.
 * @vr: The #VirguleReq context.
 * @user: The username.
 *
 * Renders the beginning of the cert level, currently by beginning a table
 * with the corresponding background color. Rendering goes to vr->b.
 **/
void
render_cert_level_begin (VirguleReq *vr, const char *user, CertStyle cs)
{
  Buffer *b = vr->b;
  const char *level_color[] = { "#c0c0c0", "#c0ffc8", "#c0d0ff", "#e0d0ff",
  "#e0d0c0" };
  const char *dark_color[] = { "#606060", "#008000", "#2040ff", "#8000c0",
  "#804020" };
  const char *level;
  CertLevel cert_level;

  if (!strcmp (user, "advogato"))
    cert_level = 4;
  else
    {
      level = req_get_tmetric_level (vr, user);
      cert_level = cert_level_from_name (level);
    }
  buffer_printf (b, "<table cellpadding=1 cellspacing=0 border=0><tr><td bgcolor=\"%s\">",
		 dark_color[cert_level]);

  buffer_printf (b, "<table cellpadding=%d cellspacing=0 border=0><tr><td bgcolor=\"%s\">\n",
		 cs == CERT_STYLE_LARGE ? 3 : 1,
		 level_color[cert_level]);
  render_table_open (vr);
}

/**
 * render_cert_level_end: End rendering cert level.
 * @vr: The #VirguleReq context.
 * @level: The cert level, as a string.
 *
 * Closes the html opened by render_cert_level_begin().
 **/
void
render_cert_level_end (VirguleReq *vr, CertStyle cs)
{
  Buffer *b = vr->b;

  render_table_close (vr);
  buffer_puts (b, "</td></tr></table>");
  buffer_puts (b, "</td></tr></table>\n");
}
