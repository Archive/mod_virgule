array_header *
tmetric_run (VirguleReq *vr,
	     const char *seeds[], int n_seeds,
	     const int *caps, int n_caps);

int
tmetric_serve (VirguleReq *vr);

char *tmetric_get (VirguleReq *vr);
