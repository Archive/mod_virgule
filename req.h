/* A data structure that persists across requests. */

typedef struct _VirguleReq VirguleReq;

struct _VirguleReq {
  request_rec *r;
  Buffer *b;
  Db *db;
  char *uri;
  const char *u; /* authenticated username */
  char *args;
  DbLock *lock;
  int raw; /* TRUE if there are no enclosing <blockquote>'s */
  int sitemap_rendered; /* TRUE if the sitemap has already been rendered */
  char *tmetric;
  char *prefix; /* Prefix of <Location> directive, to be added to links */
  table *render_data;
};

int
send_response (VirguleReq *vr);

table *
get_args_table (VirguleReq *vr);

char *
req_get_tmetric (VirguleReq *vr);

const char *
req_get_tmetric_level (VirguleReq *vr, const char *u);

int
req_ok_to_post (VirguleReq *vr);
