/*
 * mod_virgule.c -- Apache module for building community sites
 *
 * Copyright 1999 Raph Levien <raph@acm.org>
 *
 * Released under GPL v2.
 */ 

#include <string.h>

/* gnome-xml includes */
#include <parser.h>
#include <tree.h>

#include "httpd.h"
#include "http_core.h"
#include "http_config.h"
#include "http_protocol.h"
#include "ap_config.h"

#include "buffer.h"
#include "db.h"
#include "req.h"
#include "site.h"
#include "apache_util.h"
#include "main_page.h"
#include "acct_maint.h"
#include "diary.h"
#include "article.h"
#include "proj.h"
#include "rss_export.h"
#include "style.h"
#include "tmetric.h"
#include "auth.h"
#include "db_ops.h"
#include "xmlrpc.h"

typedef struct {
  char *dir;
  char *db;
} virgule_dir_conf;

module MODULE_VAR_EXPORT virgule_module;

static void *
create_virgule_dir_conf (pool *p, char *dir)
{
  virgule_dir_conf *result = (virgule_dir_conf *)ap_palloc (p, sizeof (virgule_dir_conf));

  result->dir = ap_pstrdup (p, dir);
  result->db = NULL;
  return result;
}

static const char *
set_virgule_db (cmd_parms *parms, void *mconfig, char *db)
{
  virgule_dir_conf *cfg = (virgule_dir_conf *)mconfig;

  cfg->db = (char *)ap_pstrdup (parms->pool, db);
  return NULL;
}

static int header_trace (void *data, const char *key, const char *val)
{
  Buffer *b = (Buffer *)data;
  buffer_printf (b, "<dd>%s: <tt>%s</tt>\n", key, val);
  return 1;
}

static void
render_doc (Buffer *b, Db *db, const char *key)
{
  char *buf;
  int buf_size;
  xmlDocPtr doc;
  xmlNodePtr root;

  buf = db_get (db, key, &buf_size);
  if (buf == NULL)
    {
      buffer_append (b, "Error reading key ", key, "\n", NULL);
    }
  else
    {
      doc = xmlParseMemory (buf, buf_size);
      if (doc != NULL)
	{
	  root = doc->root;
	  buffer_printf (b, "&lt;%s&gt;\n", root->name);
	  xmlFreeDoc (doc);
	}
      else
	buffer_append (b, "Error parsing key ", key, "\n", NULL);
    }
}

static void
count (pool *p, Buffer *b, Db *db, const char *key)
{
  char *buf;
  int buf_size;
  int count;

  buf = db_get (db, key, &buf_size);
  if (buf == NULL)
    count = 0;
  else
    count = atoi (buf);

  count++;
  buffer_printf (b, "<p> This page has been accessed %d time%s. </p>\n",
		 count, count == 1 ? "" : "s");
  buf = ap_psprintf (p, "%d\n", count);
#if 0
  /* useful for testing locking */
  sleep (5);
#endif
  if (db_put (db, key, buf, strlen (buf)))
    buffer_puts (b, "Error updating count\n");
}

char *old_filename;

static int
test_page (VirguleReq *vr)
{
  request_rec *r = vr->r;
  virgule_dir_conf *cfg = (virgule_dir_conf *)ap_get_module_config (r->per_dir_config, &virgule_module);
  Buffer *b = vr->b;
  Db *db = vr->db;
  table *args_table;

  char *args;

  r->content_type = "text/html";

  buffer_puts(b, "<html><head><title>\n"
	      "Virgule\n"
	      "</title></head>\n"
	      "<body bgcolor=white>");
  buffer_puts (b, "<h1>Virgule test</h1>\n");
  buffer_printf (b, "<p> The uri is: <tt>%s</tt> </p>\n", r->uri);
  buffer_printf (b, "<p> The document root is: <tt>%s</tt> </p>\n", ap_document_root (r));
  buffer_printf (b, "<p> The filename is: <tt>%s</tt> </p>\n", r->filename);
  buffer_printf (b, "<p> The old filename was: <tt>%s</tt> </p>\n", old_filename);
  buffer_printf (b, "<p> The path_info is: <tt>%s</tt> </p>\n", r->path_info);
  if (cfg)
    buffer_printf (b, "<p> cfg->db=\"%s\", cfg->dir=\"%s\"\n",
		   cfg->db, cfg->dir);
  args = vr->args;
  if (args)
    {
      const char *key;

      buffer_printf (b, "<p> The args are: <tt>%s</tt> </p>\n", args);
      args_table = get_args_table (vr);
      key = ap_table_get (args_table, "key");
      if (key)
	{
	  add_recent (r->pool, db, "test/recent.xml", key, 5);
	  buffer_printf (b, "<p> The translation of key %s is %s\n",
			 key, db_mk_filename (vr->db, key));
	}
    }

  auth_user (vr);
  if (vr->u)
    buffer_printf (b, "<p> The authenticated user is: <tt>%s</tt> </p>\n",
		   vr->u);
  
  buffer_puts (b, "<dl><dt>Headers in:\n");
  ap_table_do (header_trace, b, r->headers_in, NULL);
  buffer_puts (b, "</dl>\n");
  
  buffer_puts (b, "<dl><dt>Headers out:\n");
  ap_table_do (header_trace, b, r->headers_out, NULL);
  buffer_puts (b, "</dl>\n");
  
  render_doc (b, db, "test.xml");
  
  count (r->pool, b, db, "misc/admin/counter/count");
  
  buffer_puts (b, "</body></html>\n");

  return send_response (vr);
}

static int
test_serve (VirguleReq *vr)
{
  int max;
  char *new_key;

  if (strcmp (vr->r->uri, "/test.html"))
    return DECLINED;
  max = db_dir_max (vr->db, "test/inc");
  new_key = ap_psprintf (vr->r->pool, "test/inc/_%d", max + 1);
  db_put (vr->db, new_key, new_key, strlen (new_key));
  return send_error_page (vr, "Test page",
			  "This is a test, max = %d, new_key = %s.",
			  max, new_key);
}

/* The sample content handler */
static int virgule_handler(request_rec *r)
{
  virgule_dir_conf *cfg = (virgule_dir_conf *)ap_get_module_config (r->per_dir_config, &virgule_module);
  Buffer *b = buffer_new (r->pool);
  Db *db;
  int status;
  VirguleReq *vr;

  db = db_new_filesystem (r->pool, cfg->db); /* hack */

  vr = (VirguleReq *)ap_pcalloc (r->pool, sizeof (VirguleReq));

  vr->r = r;
  vr->b = b;
  vr->db = db;
  if (cfg->dir && !strncmp (r->uri, cfg->dir, strlen (cfg->dir)))
    {
      vr->prefix = cfg->dir;
      vr->uri = r->uri + strlen (cfg->dir);
    }
  else
    {
      vr->prefix = "";
      vr->uri = r->uri;
    }

  vr->u = NULL;
  vr->args = get_args (r);

  vr->lock = NULL;
  vr->tmetric = NULL;
  vr->sitemap_rendered = 0;
  vr->render_data = ap_make_table (r->pool, 4);

  if (!strcmp (r->uri, "/foo.html"))
    return test_page (vr);

  vr->lock = db_lock (db);
  if (vr->lock == NULL)
    return send_error_page (vr, "Lock error",
			    "There was an error acquiring the lock, %s.",
			    strerror (errno));

#if 0
  ap_table_add (r->headers_out, "Set-Cookie", "u=raph; path=/; Expires=Wednesday, 09-Nov-99 23:12:40 GMT");
  ap_table_add (r->headers_out, "Set-Cookie", "p=password3; path=/; Expires=Wednesday, 09-Nov-99 23:12:40 GMT");
#endif

  status = xmlrpc_serve (vr);
  if (status != DECLINED)
    return status;  
  
  status = site_serve (vr);
  if (status != DECLINED)
    return status;

  status = acct_maint_serve (vr);
  if (status != DECLINED)
    return status;

  status = tmetric_serve (vr);
  if (status != DECLINED)
    return status;

  status = test_serve (vr);
  if (status != DECLINED)
    return status;

  status = diary_serve (vr);
  if (status != DECLINED)
    return status;

  status = article_serve (vr);
  if (status != DECLINED)
    return status;

  status = proj_serve (vr);
  if (status != DECLINED)
    return status;

  status = rss_serve (vr);
  if (status != DECLINED)
    return status;

  if (!strcmp (r->uri, "/"))
    return main_page (vr);

  return NOT_FOUND;
}

static int
xlat_handler (request_rec *r)
{
  virgule_dir_conf *cfg = (virgule_dir_conf *)ap_get_module_config (r->per_dir_config, &virgule_module);
  const char *exceptions[] = { "/images/" };

  if (cfg->db)
    {
      int i;

      for (i = 0; i < sizeof(exceptions) / sizeof(exceptions[0]); i++)
        if (!strncmp (r->uri, exceptions[i], strlen (exceptions[i])))
          return DECLINED;

      old_filename = r->filename;
      r->handler = "virgule";
      r->filename = ap_pstrdup (r->pool, cfg->db);

#if 0
      /* strip <Location> prefix from url */
      if (cfg->dir && !strncmp (r->uri, cfg->dir, strlen (cfg->dir)))
	r->uri = ap_pstrdup (r->pool, r->uri + strlen (cfg->dir));
#endif

      return OK;
    }
  else
    return DECLINED;

}

/* Dispatch list of content handlers */
static const handler_rec virgule_handlers[] = { 
    { "virgule", virgule_handler }, 
    { NULL, NULL }
};

static const command_rec virgule_cmds[] =
{
  {"VirguleDb", set_virgule_db, NULL, OR_ALL, TAKE1, "the virgule database"},
  {NULL}
};

/* Dispatch list for API hooks */
module MODULE_VAR_EXPORT virgule_module = {
    STANDARD_MODULE_STUFF, 
    NULL,                  /* module initializer                  */
    create_virgule_dir_conf,      /* create per-dir    config structures */
    NULL,                  /* merge  per-dir    config structures */
    NULL,                  /* create per-server config structures */
    NULL,                  /* merge  per-server config structures */
    virgule_cmds,          /* table of config file commands       */
    virgule_handlers,      /* [#8] MIME-typed-dispatched handlers */
    xlat_handler,          /* [#1] URI to filename translation    */
    NULL,                  /* [#4] validate user id from request  */
    NULL,                  /* [#5] check if the user is ok _here_ */
    NULL,                  /* [#3] check access by host address   */
    NULL,                  /* [#6] determine MIME type            */
    NULL,                  /* [#7] pre-run fixups                 */
    NULL,                  /* [#9] log a transaction              */
    NULL,                  /* [#2] header parser                  */
    NULL,                  /* child_init                          */
    NULL,                  /* child_exit                          */
    NULL                   /* [#0] post read-request              */
};

