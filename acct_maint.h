char *
acct_dbkey (pool *p, const char *u);

int
acct_login (VirguleReq *vr, const char *u, const char *pass,
	    const char **ret1, const char **ret2);

int
acct_maint_serve (VirguleReq *vr);
